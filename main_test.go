package main

import (
	"testing"
)

func TestAdd5(t *testing.T) {
	tests := []struct{
		name string
		in int
		out int
	} {
		{
			"basic",
			1,
			6,
		},
		{
			"0",
			0,
			5,
		},
		{
			"negative",
			-1,
			4,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			actual := Add5(tc.in)
			if actual != tc.out {
				t.Errorf("Bad Add5 for %d, expected: %d, actual: %d", tc.in, tc.out, actual)
			}
		})
	}
}
